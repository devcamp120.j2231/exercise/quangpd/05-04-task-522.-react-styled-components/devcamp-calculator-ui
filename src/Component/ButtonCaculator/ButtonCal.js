import styled from "styled-components";

const Button = styled.button`
    background-color : ${props => props.bgColor || "#0e345c"} ;
    width : 60px;
    height : 60px;
    color :${props => props.fontColor || "white"};
`
export {Button}