import styled from "styled-components";
import { Button } from "./ButtonCaculator/ButtonCal";
import StyleC from "../App.module.css";
const Caculator = () =>{
    return(
        <>
           <div className={StyleC.Calculator} >
                <h2 className="text-white">Caculator</h2>
                <p className="text-white">2536 + 419 + </p>
                <hr style={{color : "white" , width : "50%" ,marginLeft : "70px"}}></hr>
                <h1 className="text-white">2955</h1>
                <div className={StyleC.CaculatorButton}>
                    <div>
                        <Button fontColor = "#d45954" bgColor = "#404d5e">C</Button>
                        <Button bgColor = "#404d5e">=/</Button>
                        <Button bgColor = "#404d5e">%</Button>
                        <Button bgColor = "#404d5e">/</Button>
                    </div>
                    <div>
                        <Button>7</Button>
                        <Button>8</Button>
                        <Button>9</Button>
                        <Button bgColor = "#404d5e">X</Button>
                    </div>
                    <div>
                        <Button>4</Button>
                        <Button>5</Button>
                        <Button>6</Button>
                        <Button bgColor = "#404d5e">-</Button>
                    </div>
                    <div>
                        <Button>1</Button>
                        <Button>2</Button>
                        <Button>3</Button>
                        <Button bgColor = "#404d5e">+</Button>
                    </div>
                    <div>
                        <Button>.</Button>
                        <Button>0</Button>
                        <Button>{'<'}</Button>
                        <Button bgColor = "#404d5e">=</Button>
                    </div>
                </div>
           </div>
        </>
    )
}

export default Caculator